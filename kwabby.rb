require 'json'
require 'net/http'

class Wanikani
  attr_accessor :api_key

  @@base_url = 'https://www.wanikani.com/api/user/'

  def initialize(api_key)
    @api_key = api_key
  end

  def api_url(type, params=nil)
    "#{@@base_url}#{@api_key}/#{type}/#{params}"
  end

  def api_call(type, params=nil)
    data = nil
    uri = URI(self.api_url(type, params))
    Net::HTTP.start(uri.host, uri.port,
                    :use_ssl => uri.scheme == 'https') do |http|
      request = Net::HTTP::Get.new uri
      resp = http.request request
      case resp
      when Net::HTTPSuccess
        data = JSON.parse(resp.body)
        if data.has_key? 'requested_information' and !data['requested_information'].nil?
          data = data['requested_information'] or data
        end
      end
    end
    data
  end

  def kanji(params = nil)
    self.api_call 'kanji', params
  end

  def vocabulary(params = nil)
    self.api_call 'vocabulary', params
  end

  def radicals(params = nil)
    self.api_call 'radicals', params
  end

  def recent_unlocks(params = nil)
    self.api_call 'recent-unlocks', params
  end

  def critical_items(params = nil)
    self.api_call 'critical-items', params
  end

  def srs_distribution
    self.api_call 'srs-distribution'
  end

  def level_progression
    self.api_call 'level-progression'
  end

  def study_queue
    self.api_call 'study-queue'
  end

  def user_information
    self.api_call 'user-information'
  end
end
